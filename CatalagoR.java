/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Catalogo;

import Datos.Idatos;
import Datos.datos;
import domain.DatosEntidad;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author kevin
 */
@WebServlet("/CatalagoR")
public class CatalagoR extends HttpServlet{
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
      
    }
    
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        Idatos usuario = new datos();
        
        //
        DatosEntidad usuariojsp = new DatosEntidad();
        
        
        // insertar
        usuariojsp.setNombreTienda(request.getParameter("nombreT"));
        usuariojsp.setColonia(request.getParameter("nombreC"));
        usuariojsp.setCalle(request.getParameter("nombreCa"));
        usuariojsp.setProductoDeTienda(request.getParameter("tipo"));
        try {
            if(usuario.insertar(usuariojsp) != 1){
                System.out.println("No se agrego el usuario");
            }else{
                System.out.println("El usuario fue agregado");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CatalagoR.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.print("<head>");
        out.print("<link href=\"estilos.css\" rel=\"stylesheet\" type=\"text/css\"/>");
        out.print("</head>");
        out.print("<h1>");
        out.print("Catálogo de tienda online");
        out.print("</h1>");
        out.print("<h2>");
        out.print("Los datos se registraron correctamente");
        out.print("</h2>");
        out.print("<li><a href=\"index.html\">Ir al inicio</a></li>");
        out.println("</body>");
        out.println("</html>");
    }
    
}
