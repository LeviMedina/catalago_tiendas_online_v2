/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import domain.UsuariosEntidad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author kevin
 */
public class usuarios implements IUsuarios {

    private static final String SQLSELECTPORID = "SELECT ID_Usuario, login, contraseña,"
            + "Correo, NombreDeUsuario FROM usuarios WHERE ID_Usuario = ?";
    
    private static final String SQLSELECT = "SELECT ID_Usuario, login, contraseña,"
            + "Correo, NombreDeUsuario FROM usuarios";
    
    private static final String SQLINSERT = "INSERT INTO usuarios(login, contraseña,"
            + "Correo, NombreDeUsuario) VALUES (?,?,?,?)";
    
    private static final String SQLDELETE = "DELETE FROM usuarios WHERE ID_Usuario = ?";
    
    private static final String SQLUPDATE = "UPDATE usuarios "
            + "SET login = ?,"
            + "contraseña = ?,"
            + "Correo = ?"
            + "NombreDeUsuario = ?"
            + "WHERE ID_Usuario = ?";
    

    @Override
    public List<UsuariosEntidad> seleccionar() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int insertar(UsuariosEntidad usuario) throws SQLException {
    
        Connection conn = null;
        PreparedStatement stmt = null;
        int filasAfectadas = 0;

        try {
            conn = Conexion.getConnection();             
            
            stmt = conn.prepareStatement(SQLINSERT);    
            stmt.setString(1, usuario.getLogin());
            stmt.setString(2, usuario.getContraseña());
            stmt.setString(3, usuario.getCorreo());
            stmt.setString(4, usuario.getNombreDeUsuario());
            
            filasAfectadas = stmt.executeUpdate();         
        } catch (SQLException e) {
            e.printStackTrace(System.out);
        }
        finally {           
            stmt.close();
            conn.close();
        }
        return filasAfectadas;    
    }

    @Override
    public int eliminar(UsuariosEntidad usuario) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public UsuariosEntidad buscar(UsuariosEntidad usuario) {
        // Buscar alumno por ID

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Conexion.getConnection();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try {
            stmt = conn.prepareStatement(SQLSELECTPORID);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try {
            stmt.setInt(1, usuario.getID_Usuario());
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try {
            rs = stmt.executeQuery();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try {
            // validar
            if (rs.next()) {
                // Leer valores del registro
                String login = rs.getString("login");
                String contraseña = rs.getString("contraseña");
                String Correo = rs.getString("Correo");
                String NombreDeUsuario = rs.getString("NombreDeUsuario");
                

                // Pasar variables al objeto de retorno
                usuario.setLogin(login);
                usuario.setContraseña(contraseña);
                usuario.setCorreo(Correo);
                usuario.setNombreDeUsuario(NombreDeUsuario);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        try {
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        try {
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return usuario;
    }

    @Override
    public int actualizar(UsuariosEntidad usuario) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}