/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author kevin
 */
public class DatosEntidad {
    private int ID_Tienda;
    private String NombreTienda;
    private String Colonia;
    private String Calle;
    private int ID_Usuario;
    private String ProductoDeTienda;
    
    public DatosEntidad(){
    
    }
    
    public DatosEntidad(int ID_Tienda, String NombreTienda, String Colonia, String Calle, int ID_Usuario, String ProductoDeTienda){
    this.ID_Tienda = ID_Tienda;
    this.NombreTienda = NombreTienda;
    this.Colonia = Colonia;
    this.Calle = Calle;
    this.ID_Usuario = ID_Usuario;
    this.ProductoDeTienda = ProductoDeTienda;
    }
    
    public DatosEntidad(String NombreTienda, String Colonia, String Calle, int ID_Usuario, String ProductoDeTienda){
    this.NombreTienda = NombreTienda;
    this.Colonia = Colonia;
    this.Calle = Calle;
    this.ID_Usuario = ID_Usuario;
    this.ProductoDeTienda = ProductoDeTienda;
    }
    
    public DatosEntidad(int ID_Tienda){
        this.ID_Tienda = ID_Tienda;
    }

    public int getID_Tienda() {
        return ID_Tienda;
    }

    public void setID_Tienda(int ID_Tienda) {
        this.ID_Tienda = ID_Tienda;
    }

    public String getNombreTienda() {
        return NombreTienda;
    }

    public void setNombreTienda(String NombreTienda) {
        this.NombreTienda = NombreTienda;
    }

    public String getColonia() {
        return Colonia;
    }

    public void setColonia(String Colonia) {
        this.Colonia = Colonia;
    }

    public String getCalle() {
        return Calle;
    }

    public void setCalle(String Calle) {
        this.Calle = Calle;
    }

    public int getID_Usuario() {
        return ID_Usuario;
    }

    public void setID_Usuario(int ID_Usuario) {
        this.ID_Usuario = ID_Usuario;
    }

    public String getProductoDeTienda() {
        return ProductoDeTienda;
    }

    public void setProductoDeTienda(String ProductoDeTienda) {
        this.ProductoDeTienda = ProductoDeTienda;
    }
    
     @Override
    public String toString() {
        return "datos{" + "ID_Tienda=" + ID_Tienda + ", NombreTienda=" + NombreTienda + ", Colonia=" + Colonia + ", Calle=" + Calle + ", ID_Usuario=" + ID_Usuario + ", ProductoDeTienda="+ ProductoDeTienda + '}';
    }
}
