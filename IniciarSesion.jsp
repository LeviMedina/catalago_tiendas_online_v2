<%-- 
    Document   : IniciarSesion
    Created on : 23/11/2020, 08:56:48 AM
    Author     : pima_
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Iniciar Sesion</title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>Catálogo de tiendas online</h1>
        <nav>
        <ul>
            <li><a href="index.jsp">Inicio</a></li>
            <li><a href="BuscarT.jsp">Buscar tiendas cercanas</a></li>
            <li><a href="Registro.jsp">Registrarse</a></li>
        </ul>
        </nav>
        <div class="container well" id="containerLogin"> 
            <h3 class="modal-title">Iniciar Sesion</h3>
            <center> <img src="17004.png" alt="" class="img-rounded"/> </center>
           				
                        	
                    

            <!-- bootstrap -->
            <form id="form1">
  <div class="modal-body ">
                        <form action="${pageContext.request.contextPath}/ServletUsuario?accion=ingresar" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Username" required="required" autofocus>		
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="required">	
                            </div>        
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn" id="botonLogin">Ingresar</button>
                            </div>
                        </form>
                            <div class="modal-footer">
                        <a href="#">¿Olvidó su contraseña?</a>
                        <p>UAC - Sistemas II</p>
                    </div>
        </div>
        </div>
    </body>
</html>
