/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import domain.DatosEntidad;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author kevin
 */
public interface Idatos {
    public List<DatosEntidad> seleccionar();
    public int insertar(DatosEntidad usuario)throws SQLException;
    public int eliminar(DatosEntidad usuario);
    public DatosEntidad buscar(DatosEntidad usuario);
    public int actualizar(DatosEntidad usuario);
}
