/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;


import domain.UsuariosEntidad;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author kevin
 */
public interface IUsuarios {
    public List<UsuariosEntidad> seleccionar();
    public int insertar(UsuariosEntidad usuario)throws SQLException;
    public int eliminar(UsuariosEntidad usuario);
    public UsuariosEntidad buscar(UsuariosEntidad usuario);
    public int actualizar(UsuariosEntidad usuario);
}
