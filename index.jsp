<%-- 
    Document   : index
    Created on : 23/11/2020, 01:06:47 PM
    Author     : pima_
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu</title>
       <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <h1>Catálogo de tiendas online</h1>
        
       
         <nav>
        <ul>
            <li><a href="IniciarSesion.jsp">Iniciar Sesión</a></li>
            <li><a href="BuscarT.jsp">Buscar tiendas cercanas</a></li>
            <li><a href="Registro.jsp">Registrarse</a></li>
        </ul>
        </nav>
        <h4>Aquí podrás encontrar las tiendas registradas en esta página, que se encuentran cerca de ti</h4>
        <div class="content-all">
       <div class="content-carrousel">
           <figure><img src="image/logo2.png"></figure>
           <figure><img src="image/telefono-catalogo-tienda-online-ropa-busqueda-productos-pagina-azul_249405-71 (6).jpg"></figure>
           <figure><img src="image/concepto-comercio-electronico_23-2147505751.jpg"></figure>
           <!--<figure><img src="image/img3.jpg"></figure>
           <figure><img src="image/img4.jpg"></figure>
           <figure><img src="image/img5.jpg"></figure>
           <figure><img src="image/img6.jpg"></figure>
           <figure><img src="image/img7.jpg"></figure>-->
           
       </div>        
    </div>
        
       <!--
          <div class="container well" id="containerMenu"> 
           <div class="card" style="width: 18rem;">
               <center><img src="logo2.png" class="img-rounded" alt="..." ></center>
  <div class="card-body">
    <p class="card-text">Aquí podrás encontrar las tiendas registradas en esta página, que se encuentran cerca de ti</p>
  </div>
</div>-->
    </body>
</html>
